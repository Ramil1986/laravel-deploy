# Инфраструктура

На Хост машине произвести установку приложений:
- ansible
- vagrant
- virtualbox
- gitlab-runner


Скопировать файл Vagrantfile и playbook-vagrant.yml. В Vagrantfile указаны ip-адреса создаваемых виртуальных машин:

app01 192.168.0.151 

db01 192.168.0.152 

vpn01 192.168.0.153 

gitlab01 192.168.0.154 

prod01 192.168.0.155 


Данные по машинам также указаны в файле hosts.


Выполнить команду vagrant up. Произойдет настройка виртуальных машин необходимых для работы приложения.


На виртуальной машине gitlab01 произвести установку локального gitlab согласно инструкции https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-gitlab-on-ubuntu-18-04-ru. Затем создать проект и залить код приложения https://gitfront.io/r/deusops/2cL1ba8fzWDo/laravel-project-travellist/.


## Vars

Для корректной работы приложения предусмотрены следующие VARSы:   
| Название                    | Значение по умолчанию | Описание                                    |
|-----------------------------|-----------------------|---------------------------------------------|
| `php_version`               | `php7.4`              | версия php                                  |
| `app_directory`             | `travel_list`         | адрес виртуальной машины с локальным gitlab |
| `gitlab_address`            | '192.168.0.154`       | адрес машины с локальным gitlab             |
| `openvpn_easyrsa_dir`       | `/usr/share/easy-rsa` | Путь до приложения easy-rsa                 |  
| `openvpn_dir`               | `/etc/openvpn`        | Путь до приложения openvpn                  |
| `openvpn_port`              | `1194`                | Порт openvpn                                |
| `openvpn_proto`             | `udp`                 | Протокол openvpn                            |
| `openvpn_network`           | `192.168.0.0`         | Сеть openvpn                                |
| `openvpn_netmask`           | `255.255.255.0`       | Маска сети openvpn                          |
| `openvpn_address`           | `{{ ansible_default_ipv4.address }}`| Ip-адрес сервера openvpn      | 
| `openvpn_clients`           | `- Ramil_Nizamov`     | Список клиентов openvpn                     |
| `db_user`                   | `travel_user`         | Имя пользователя базы данных                |
| `db_password`               | `app01user`           | Пароль пользователя базы данных             |		
| `db_connection`             | `pgsql`               | Тип базы данных                             |
| `db_host`                   | `192.168.0.152`       | Ip-адрес машины с базой данный              |		
| `db_port`                   | `5432`                | Порт базы данных                            |		
| `postgresql_version`        | `12`                  | Версия базы данных                          |		
| `postgresql_bin_dir`        | `/usr/lib/postgresql/{{ postgresql_version }}/bin` | Директория приложения базы            |		
| `postgresql_data_dir`       | `/var/lib/postgresql/{{ postgresql_version }}/main`| Директория конфигурационных файлов базы данных                             |		
